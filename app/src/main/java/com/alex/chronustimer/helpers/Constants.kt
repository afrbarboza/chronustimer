package com.alex.chronustimer.helpers

class Constants private constructor() {
    companion object {
        const val FAKE_REQUEST_DELAY: Long = 300L
        const val NO_TASK_TO_TRACK = -1
    }
}