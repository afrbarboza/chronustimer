package com.alex.chronustimer.repository

import com.alex.chronustimer.helpers.Constants.Companion.FAKE_REQUEST_DELAY
import com.alex.chronustimer.model.TaskModel
import kotlinx.coroutines.delay
import kotlin.random.Random

class TasksRepository private constructor() {

    companion object  {

        private var tasksList = ArrayList<TaskModel>()

        suspend fun fetchTasks(): ArrayList<TaskModel> {
            delay(FAKE_REQUEST_DELAY)
            return this.tasksList
        }

        suspend fun addNewTask(taskName: String): ArrayList<TaskModel> {
            val taskId = Random.nextInt(0, 10000)
            delay(FAKE_REQUEST_DELAY)
            this.tasksList.add(TaskModel(taskId, taskName))
            return this.tasksList
        }

        suspend fun fetchTaskById(taskId: Int): TaskModel {
            delay(FAKE_REQUEST_DELAY)
            return this.tasksList.first {
                it.taskId == taskId
            }
        }

    }
}