package com.alex.chronustimer.view

import android.os.Bundle
import android.os.SystemClock
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import com.alex.chronustimer.databinding.FragmentTimerBinding
import com.alex.chronustimer.model.TaskModel
import com.alex.chronustimer.viewmodel.TimerViewModel

class TimerFragment : Fragment() {

    private lateinit var timerViewModel: TimerViewModel

    private var _binding: FragmentTimerBinding? = null
    private val binding get() = _binding!!

    private var taskIdToTrack: Int = 0

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentTimerBinding.inflate(inflater, container, false)
        initViewModel()
        initObservers()
        initViewData()
        initViews()
        return binding.root
    }

    private fun initViewModel() {
        timerViewModel = ViewModelProvider(this).get(TimerViewModel::class.java)
    }

    private fun initObservers() {
        timerViewModel.currentTrackedTask.observe(viewLifecycleOwner) {
            if (isValidTaskToTrackTime(it)) {
                initChronometerBaseTime()
                handleFlowTaskTimeTracker(it)
            } else {
                handleFlowChronometerOnly()
            }
        }
    }

    private fun initViewData() {
        this.taskIdToTrack = TimerFragmentArgs.fromBundle(requireArguments()).taskIdToTrack
        timerViewModel.fetchCurrentTask(this.taskIdToTrack)
    }

    private fun isValidTaskToTrackTime(task: TaskModel): Boolean {
        return timerViewModel.isValidTaskToTrackTime(task.taskId)
    }

    private fun handleFlowTaskTimeTracker(task: TaskModel) {
        binding.tvTaskTitle.visibility = View.VISIBLE
        binding.tvTaskTitle.text = task.taskName
    }

    private fun handleFlowChronometerOnly() {
        binding.tvTaskTitle.visibility = View.INVISIBLE
    }

    private fun initViews() {
        binding.toggleChronometer.setOnClickListener {
            if (binding.toggleChronometer.isChecked) {
                /* start */
                initChronometerBaseTime()
                binding.chronometerTask.start()
            } else {
                /* stop */
                timerViewModel.setSpentTimeInCurrentTask(SystemClock.elapsedRealtime() - binding.chronometerTask.base)
                binding.chronometerTask.stop()
            }
        }
    }

    private fun initChronometerBaseTime() {
        binding.chronometerTask.base = timerViewModel.getSpentTimeInCurrentTask()
    }
}