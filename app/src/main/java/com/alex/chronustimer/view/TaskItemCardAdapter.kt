package com.alex.chronustimer.view

import android.os.SystemClock
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.alex.chronustimer.R
import com.alex.chronustimer.databinding.TaskCardItemBinding
import com.alex.chronustimer.model.TaskModel

class TaskItemCardAdapter(private var allTasks: ArrayList<TaskModel>,
                          private val taskCardClickHandler: NavigateToTimerCallback):
    RecyclerView.Adapter<TaskItemViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TaskItemViewHolder {
        val layoutInflater: LayoutInflater = LayoutInflater.from(parent.context)
        val binding: TaskCardItemBinding = DataBindingUtil
            .inflate(layoutInflater, R.layout.task_card_item, parent,false)
        return TaskItemViewHolder(binding)
    }

    override fun onBindViewHolder(holder: TaskItemViewHolder, position: Int) {
        holder.bind(allTasks[position], taskCardClickHandler)
    }

    override fun getItemCount(): Int = allTasks.size

    fun updateData(newTasks: ArrayList<TaskModel>) {
        this.allTasks = newTasks
        this.notifyDataSetChanged()
    }
}

class TaskItemViewHolder(private val binding: TaskCardItemBinding):
    RecyclerView.ViewHolder(binding.root) {

    fun bind(taskItem: TaskModel, taskCardClickHandler: NavigateToTimerCallback) {
        binding.taskCardName.text = taskItem.taskName
        binding.taskCardTimeSpent.base = SystemClock.elapsedRealtime() - taskItem.baseTime
        binding.taskCardTouchArea.setOnClickListener {
            taskCardClickHandler.onTaskCardClicked(taskItem.taskId)
        }
    }
}
