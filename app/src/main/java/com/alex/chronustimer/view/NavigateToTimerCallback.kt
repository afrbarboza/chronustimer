package com.alex.chronustimer.view

interface NavigateToTimerCallback {
    fun onTaskCardClicked(taskId: Int)
}