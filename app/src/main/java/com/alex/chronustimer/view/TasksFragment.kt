package com.alex.chronustimer.view

import android.graphics.drawable.Animatable
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.vectordrawable.graphics.drawable.AnimatedVectorDrawableCompat
import com.alex.chronustimer.R
import com.alex.chronustimer.databinding.FragmentTasksBinding
import com.alex.chronustimer.model.TaskModel
import com.alex.chronustimer.viewmodel.TasksListViewModel
import kotlinx.coroutines.*

class TasksFragment : Fragment() {

    private var _binding: FragmentTasksBinding? = null
    private val binding get() = _binding!!

    private lateinit var tasksListViewModel: TasksListViewModel

    private var taskItemCardAdapter: TaskItemCardAdapter? = null

    private var glassHourRotationAnimationJob: Job? = null

    private var glassHourRotationAnimation: AnimatedVectorDrawableCompat? = null

    private var glassHourFadeInAnimation: Animation? = null

    companion object {
        const val ROTATION_DELAY: Long = 6000
        const val TRANSITION_DELAY: Long = 100
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentTasksBinding.inflate(inflater, container, false)

        initViews()
        initViewModel()
        initObservers()
        initViewData()
        return binding.root
    }

    override fun onPause() {
        super.onPause()
        disableGlassHourAnimations()
    }

    private fun initViewModel() {
        tasksListViewModel = ViewModelProvider(this).get(TasksListViewModel::class.java)
    }

    private fun initObservers() {
        tasksListViewModel.tasksList.observe(viewLifecycleOwner, {
            updateTasksRecyclerView(it)
        })
    }

    private fun initViewData() {
        tasksListViewModel.fetchTasks()
    }

    private fun initViews() {
        binding.tasksRecyclerView.layoutManager = LinearLayoutManager(context)
        binding.fabNewTasks.setOnClickListener {
            val dialog = NewTaskDialog(NewTaskDialogCallbackImpl())
            dialog.show(requireActivity().supportFragmentManager, "")
        }
    }

    private fun updateTasksRecyclerView(newTasks: ArrayList<TaskModel>) {
        if (newTasks.isNullOrEmpty()) {
            handleEmptyListOfTasks()
        } else {
            handleFilledListOfTasks(newTasks)
        }
    }

    private fun handleEmptyListOfTasks() {
        makeEmptyTaskListContainerVisible()
        setupGlassHourFadeInAnimation()
        setupGlassHourRotationAnimation()
        if (isGlassHourRotationAnimationJobActive()) {
            return
        } else {
            dispatchGlassHourRotationAnimationJob()
        }
    }

    private fun handleFilledListOfTasks(newTasks: ArrayList<TaskModel>) {
        makeEmptyTaskListContainerInvisible()
        cancelGlassHourRotationAnimationJob()
        setupRecyclerViewWithFetchedTasks(newTasks)
    }

    private fun makeEmptyTaskListContainerVisible() {
        binding.tasksRecyclerView.visibility = View.GONE
        binding.emptyTaskListContainer.visibility = View.VISIBLE
    }

    private fun setupGlassHourFadeInAnimation() {
        this.glassHourFadeInAnimation = AnimationUtils.loadAnimation(requireContext(), R.anim.fade_in)
        binding.emptyTaskListContainer.startAnimation(glassHourFadeInAnimation)
    }

    private fun setupGlassHourRotationAnimation() {
        this.glassHourRotationAnimation = AnimatedVectorDrawableCompat
            .create(requireContext(), R.drawable.animated_hourglass)
        this.glassHourRotationAnimation.let {
            binding.rotationAnimation.setImageDrawable(it)
        }
    }

    private fun isGlassHourRotationAnimationJobActive() = glassHourRotationAnimationJob != null

    private fun dispatchGlassHourRotationAnimationJob() {
        glassHourRotationAnimationJob = CoroutineScope(Dispatchers.Main).launch {
            while (true) {
                (glassHourRotationAnimation as Animatable?)?.start()
                delay(ROTATION_DELAY)
                binding.rotationAnimation.visibility = View.GONE
                binding.rotationAnimation0.visibility = View.VISIBLE
                delay(TRANSITION_DELAY)
                binding.rotationAnimation0.visibility = View.GONE
                binding.rotationAnimation1.visibility = View.VISIBLE
                delay(TRANSITION_DELAY)
                binding.rotationAnimation1.visibility = View.GONE
                binding.rotationAnimation2.visibility = View.VISIBLE
                delay(TRANSITION_DELAY)
                binding.rotationAnimation2.visibility = View.GONE
                binding.rotationAnimation.visibility = View.VISIBLE
            }
        }
    }

    private fun disableGlassHourAnimations() {
        (glassHourRotationAnimation!! as Animatable?)?.stop()
        this.glassHourFadeInAnimation!!.cancel()
    }

    private fun makeEmptyTaskListContainerInvisible() {
        if (binding.emptyTaskListContainer.visibility == View.VISIBLE) {
            val fadeOutAnimation = AnimationUtils.loadAnimation(requireContext(), R.anim.fade_out)
            binding.emptyTaskListContainer.startAnimation(fadeOutAnimation)
        }

        binding.emptyTaskListContainer.visibility = View.GONE
    }

    private fun cancelGlassHourRotationAnimationJob() {
        if (isGlassHourRotationAnimationJobActive()) {
            glassHourRotationAnimationJob!!.cancel()
            glassHourRotationAnimationJob = null
        }
    }

    private fun setupRecyclerViewWithFetchedTasks(newTasks: ArrayList<TaskModel>) {
        binding.tasksRecyclerView.visibility = View.VISIBLE
        taskItemCardAdapter = TaskItemCardAdapter(newTasks, NavigateToTimerCallbackImpl())
        binding.tasksRecyclerView.adapter = taskItemCardAdapter
        taskItemCardAdapter!!.updateData(newTasks)
    }

    private inner class NewTaskDialogCallbackImpl: NewTaskDialogCallback {
        override fun onPositiveButtonClicked(taskName: String) {
            tasksListViewModel.addNewTask(taskName)
        }
    }

    private inner class NavigateToTimerCallbackImpl: NavigateToTimerCallback {
        override fun onTaskCardClicked(taskId: Int) {
            val actionTrackTaskWithId = TasksFragmentDirections
                .actionTasksFragmentToTimerFragment(taskId)
            view?.findNavController()?.navigate(actionTrackTaskWithId)
        }
    }
}
