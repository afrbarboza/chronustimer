package com.alex.chronustimer.view

interface NewTaskDialogCallback {
    fun onPositiveButtonClicked(taskName: String)
}