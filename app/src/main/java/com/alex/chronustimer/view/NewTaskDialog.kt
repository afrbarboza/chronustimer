package com.alex.chronustimer.view

import android.app.AlertDialog
import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import androidx.fragment.app.DialogFragment
import com.alex.chronustimer.R
import com.google.android.material.button.MaterialButton
import com.google.android.material.textfield.TextInputEditText
import java.lang.IllegalStateException

class NewTaskDialog(private val newTaskDialogCallback: NewTaskDialogCallback): DialogFragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.new_task_dialog, container, false)
        if (dialog != null && dialog!!.window != null) {
            dialog!!.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            dialog!!.window!!.requestFeature(Window.FEATURE_NO_TITLE)
        }
        return view
    }
    
    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return activity?.let {
            val builder = AlertDialog.Builder(it)

            val view = it.layoutInflater.inflate(R.layout.new_task_dialog, null)
            builder.setView(view)
            if (view != null) {
                initViews(view)
            }
            builder.create()
        } ?: throw IllegalStateException("Activity cannot be null")
    }

    private fun initViews(view: View) {
        val taskNameEditText = view.findViewById<TextInputEditText>(R.id.et_task_name)

        val positiveButton = view.findViewById<MaterialButton>(R.id.new_task_dialog_button)
        positiveButton.setOnClickListener {
            val nameNewTask = taskNameEditText.text.toString()
            newTaskDialogCallback.onPositiveButtonClicked(nameNewTask)
            dismiss()
        }
    }

}