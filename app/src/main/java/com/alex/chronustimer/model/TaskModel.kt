package com.alex.chronustimer.model

import com.alex.chronustimer.helpers.Constants.Companion.NO_TASK_TO_TRACK

data class TaskModel(val taskId: Int = NO_TASK_TO_TRACK,
                     val taskName: String = "",
                     var baseTime: Long = 0)