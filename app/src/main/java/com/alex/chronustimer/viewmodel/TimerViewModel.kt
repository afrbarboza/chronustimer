package com.alex.chronustimer.viewmodel

import android.os.SystemClock
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.alex.chronustimer.helpers.Constants.Companion.NO_TASK_TO_TRACK
import com.alex.chronustimer.model.TaskModel
import com.alex.chronustimer.repository.TasksRepository
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class TimerViewModel: ViewModel() {
    private val _currentTrackedTask = MutableLiveData<TaskModel>()
    val currentTrackedTask: LiveData<TaskModel> = _currentTrackedTask

    fun fetchCurrentTask(taskId: Int) {
        if (isValidTaskToTrackTime(taskId)) {
            handleFlowTaskToTrackWithId(taskId)
        } else {
            handleFlowWithNoTaskToTrack()
        }
    }

    fun getSpentTimeInCurrentTask(): Long {
        val currentOffset = currentTrackedTask.value!!.baseTime
        return SystemClock.elapsedRealtime() - currentOffset
    }

    fun setSpentTimeInCurrentTask(newSpentTime: Long) {
        _currentTrackedTask.value!!.baseTime = newSpentTime
    }

    fun isValidTaskToTrackTime(taskId: Int): Boolean  = taskId != NO_TASK_TO_TRACK

    private fun handleFlowTaskToTrackWithId(taskId: Int) {
        CoroutineScope(Dispatchers.IO).launch {
            val currentTask = TasksRepository.fetchTaskById(taskId)
            withContext(Dispatchers.Main) {
                _currentTrackedTask.value = currentTask
            }
        }
    }

    private fun handleFlowWithNoTaskToTrack() {
        val temporaryTask = TaskModel()
        _currentTrackedTask.value = temporaryTask
    }
}