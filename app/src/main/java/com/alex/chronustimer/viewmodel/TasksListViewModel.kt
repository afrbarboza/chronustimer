package com.alex.chronustimer.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.alex.chronustimer.model.TaskModel
import com.alex.chronustimer.repository.TasksRepository
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class TasksListViewModel:  ViewModel() {
    private val _tasksList = MutableLiveData<ArrayList<TaskModel>>()
    val tasksList: LiveData<ArrayList<TaskModel>>
        get() = _tasksList

    fun fetchTasks() {
        CoroutineScope(Dispatchers.IO).launch {
            val newList = TasksRepository.fetchTasks()
            withContext(Dispatchers.Main) {
                _tasksList.value = newList
            }
        }
    }

    fun addNewTask(taskName: String) {
        CoroutineScope(Dispatchers.IO).launch {
            val newList = TasksRepository.addNewTask(taskName)
            withContext(Dispatchers.Main) {
                _tasksList.value = newList
            }
        }
    }
}
